#ifndef LOGGER_H
#define LOGGER_H

#include <functional>
#include <string>
#include <vector>

#define LOG libface::Logger()

namespace libface {
    struct Logger
    {
        Logger() { }

        ~Logger();

        Logger &operator<<(int n);
        Logger &operator<<(const std::string &s);
        Logger &operator<<(const std::wstring &s);

        static std::function<void(const std::string &)> s_Callback;

    private:
        std::vector<std::string> m_LogLines;
    };
}  // namespace libface

#endif  // LOGGER_H
