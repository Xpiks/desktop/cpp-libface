#include "libfaceapi.hpp"

#include <algorithm>
#include <cassert>
#include <cctype>
#include <cstdio>
#include <cstring>
#include <ctime>
#include <fcntl.h>
#include <fstream>
#include <iostream>
#include <memory>
#include <string>
#include <sys/stat.h>

#ifdef _WIN32
#include <io.h>
#include <mman-win32/mman.h>
#else
#include <sys/mman.h>
#include <unistd.h>
#endif

#include "logger.hpp"
#include "parser.hpp"
#include "phrase_map.hpp"
#include "suggest.hpp"
#include "types.hpp"
#include "utils.hpp"

#if !defined NMAX
#define NMAX 32
#endif

#if !defined INPUT_LINE_SIZE
// Max. line size is 8191 bytes.
#define INPUT_LINE_SIZE 8192
#endif

// How many bytes to reserve for the output string
#define OUTPUT_SIZE_RESERVE 4096

// Undefine the macro below to use C-style I/O routines.
// #define USE_CXX_IO

off_t
file_size(const char *path) {
    struct stat sbuf;
    int r = stat(path, &sbuf);

    assert(r == 0);
    if (r < 0) {
        return 0;
    }

    return sbuf.st_size;
}

char
to_lowercase(char c) {
    return std::tolower(c);
}

inline void
str_lowercase(std::string &str) {
    std::transform(str.begin(), str.end(),
                   str.begin(), to_lowercase);

}

enum
{
    IMPORT_FILE_NOT_FOUND = 1,
    IMPORT_MUNMAP_FAILED  = 2,
    IMPORT_MMAP_FAILED    = 3,
    IMPORT_FILE_EMPTY     = 4,
};

bool is_EOF(FILE *pf) { return feof(pf); }
bool is_EOF(const std::ifstream &fin) { return !!fin; }

void get_line(FILE *pf, char *buff, int buff_len, int &read_len) {
    (void)buff_len;
    char *got = fgets(buff, INPUT_LINE_SIZE, pf);
    if (!got) {
        read_len = -1;
        return;
    }
    read_len = strlen(buff);
    if (read_len > 0 && buff[read_len - 1] == '\n') {
        buff[read_len - 1] = '\0';
    }
}

void get_line(std::ifstream &fin, char *buff, int buff_len, int &read_len) {
    fin.getline(buff, buff_len);
    read_len = fin.gcount();
    buff[INPUT_LINE_SIZE - 1] = '\0';
}

Souffleur::Souffleur(): if_mmap_addr(NULL), if_length(0), line_limit(-1), if_fd(-1), building(false) { }

Souffleur::~Souffleur() {
    if (if_mmap_addr) { munmap(if_mmap_addr, if_length); }
    if (if_fd != -1) { close(if_fd); }
}

int Souffleur::do_import(std::string file, uint_t limit, int &rnadded, int &rnlines) {
    bool is_input_sorted = true;
#if defined(USE_CXX_IO)
    std::ifstream fin(file.c_str());
#else
    FILE *fin = fopen(file.c_str(), "r");
#endif

    int fd = open(file.c_str(), O_RDONLY);

    DCERR("handle_import::file:" << file << "[fin: " << (!!fin) << ", fd: " << fd << "]" << std::endl);

    if (!fin || fd == -1) {
        perror("fopen");
        return -IMPORT_FILE_NOT_FOUND;
    } else {
        building = true;
        int nlines = 0;
        int foffset = 0;

        if (if_mmap_addr) {
            int r = munmap(if_mmap_addr, if_length);
            if (r < 0) {
                perror("munmap");
                building = false;
                return -IMPORT_MUNMAP_FAILED;
            }
        }

        // Potential race condition + not checking for return value
        if_length = file_size(file.c_str());
        if (if_length == 0) {
            building = false;
            return -IMPORT_FILE_EMPTY;
        }

        // mmap() the input file in
        if_mmap_addr = (char*)mmap(NULL, if_length, PROT_READ, MAP_SHARED, fd, 0);
        if (if_mmap_addr == MAP_FAILED) {
            LOG << "length:" << if_length << "fd:" << fd;
            perror("mmap");
#if !defined(USE_CXX_IO)
            if (fin) { fclose(fin); }
#endif
            if (fd != -1) { close(fd); }
            building = false;
            return -IMPORT_MMAP_FAILED;
        }

        pm.repr.clear();
        char buff[INPUT_LINE_SIZE];
        std::string prev_phrase;

        while (!is_EOF(fin) && limit--) {
            buff[0] = '\0';

            int llen = -1;
            get_line(fin, buff, INPUT_LINE_SIZE, llen);
            if (llen == -1) { break; }

            ++nlines;

            int weight = 0;
            std::string phrase;
            StringProxy snippet;
            InputLineParser(if_mmap_addr, foffset, buff, &weight, &phrase, &snippet, if_mmap_addr, if_length)
                .start_parsing();

            foffset += llen;

            if (!phrase.empty()) {
                str_lowercase(phrase);
                DCERR("Adding: " << weight << ", " << phrase << ", " << std::string(snippet) << std::endl);
                pm.insert(weight, phrase, snippet);
            }
            if (is_input_sorted && prev_phrase <= phrase) {
                prev_phrase.swap(phrase);
            } else if (is_input_sorted) {
                is_input_sorted = false;
            }
        }

        DCERR("Creating PhraseMap::Input is " << (!is_input_sorted ? "NOT " : "") << "sorted\n");

#if !defined(USE_CXX_IO)
        fclose(fin);
#endif
        pm.finalize(is_input_sorted);
        vui_t weights;
        for (size_t i = 0; i < pm.repr.size(); ++i) {
            weights.push_back(pm.repr[i].weight);
        }
        st.initialize(weights);

        rnadded = weights.size();
        rnlines = nlines;

        building = false;
        if_fd    = fd;
    }

    return 0;
}

void Souffleur::setLogger(const std::function<void(const std::string &)> &f) { libface::Logger::s_Callback = f; }

bool Souffleur::import(const char *ac_file) {
    bool result = false;
    if (ac_file) {
        int nadded = 0, nlines = 0;
        const time_t start_time = time(NULL);
        const int ret           = do_import(ac_file, line_limit, nadded, nlines);
        if (ret < 0) {
            switch (-ret) {
                case IMPORT_FILE_NOT_FOUND: LOG << "The file" << ac_file << "was not found"; break;
                case IMPORT_MUNMAP_FAILED: LOG << "munmap(2) on file" << ac_file << "failed"; break;
                case IMPORT_MMAP_FAILED: LOG << "mmap(2) on file" << ac_file << "failed"; break;
                case IMPORT_FILE_EMPTY: LOG << "file" << ac_file << "is empty"; break;
                default: LOG << "ERROR::Unknown error:" << ret;
            }

            result = false;
        } else {
            LOG << "INFO::Successfully added" << nadded << "/" << nlines << "records from" << ac_file;
            result = true;
        }
    }

    return result;
}

vp_t Souffleur::prompt(std::string prefix, uint_t n) {
    return ::suggest(pm, st, prefix, n);
}
